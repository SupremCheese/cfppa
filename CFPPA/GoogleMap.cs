﻿using GMap.NET;
using GMap.NET.MapProviders;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CFPPA
{
    class GoogleMap
    {
        //----Constante de la carte---//

        //La couleur du polygone
        private Color polygoneColor = Color.Gold;
        //Type de marqueur
        private const GMarkerGoogleType markerType = GMarkerGoogleType.orange_small;
        //Par default lat et long sur la France
        private const double latitude = 43;
        private const double longitude = 1;
        //Id par default des polygones et des marqueurs
        private const int defaultId = 1;
        private const int maxMode = 2;

        //Objet GMap
        public GMapControl carte;
        //Variable Utilitaire
        private Utils util;

        //Variables qui contient les points du polygone en cours de creation
        private List<PointLatLng> points;
        //Variables qui contient les markers
        private List<GMapOverlay> markers;
        //Variables qui contient la liste de tout les polygones
        private List<GMapPolygon> polygons;
        //Variable qui contient l'id courant du point de polygone
        private int currentMarkerId;

        //Variable qui contient le mode actuel (deplacement ou suppression)
        private int mode;

        //Constructeur de la carte
        public GoogleMap(ref GMapControl map, MouseButtons dragButton, int minZoom, int maxZoom, int currentZoom)
        {
            //On initialise l'objet Utils
            this.util = new Utils();
            //On initialise la carte
            this.carte = map;

            //On definit l'id par default pour le point du polygone
            currentMarkerId = defaultId;

            //On initialise notre variables points qui contient les points du polygone en cours de creation
            points = new List<PointLatLng>();
            //On initialise notre variables points qui contient les markers de la carte
            markers = new List<GMapOverlay>();
            //On initialise notre varibles polygons
            polygons = new List<GMapPolygon>();

            //Bouton de la souris qui sert a ce deplacer sur la carte
            this.carte.DragButton = dragButton;
            //Source de la cartographie à utiliser
            this.carte.MapProvider = GMapProviders.GoogleSatelliteMap;

            //Desactive la croix rouge au centre
            this.carte.ShowCenter = false;

            //Defini la position sur la carte
            this.carte.Position = new GMap.NET.PointLatLng(latitude, longitude);

            //Zoom min, max et par defaut
            this.carte.MinZoom = minZoom;
            this.carte.MaxZoom = maxZoom;
            this.carte.Zoom = currentZoom;

            //On initialise la variable
            this.mode = 0;
        }

        //Change le mode actuel (deplacement ou suppression)
        public void changeMode(int mode)
        {
            this.mode = mode;
            if (this.mode > maxMode)
            {
                this.mode = 0;
            }
        }

        public int getMode()
        {
            return this.mode;
        }

        //Definir la position sur la carte
        public void setPosition(double lat, double lng)
        {
            carte.Position = new GMap.NET.PointLatLng(lat, lng);
        }

        //Ajoute un point au polygone courant et l'affiche sur la carte
        public void addPolygonePoint(double lat, double lng, int polygoneId = -1)
        {
            //Contient le nom du polygone
            string markerName = "";

            //On definit le nom du marker
            if (polygoneId != -1)
            {
                markerName = "marker_" + polygoneId + "_" + currentMarkerId;
            }
            else
            {
                markerName = "marker_" + (util.getLastId() + 1) + "_" + currentMarkerId;
            }

            //Defini le type d'element a crée sur notre carte
            GMapOverlay markersOverlay = new GMapOverlay(markerName);
            //Crée le point grace aux coordonnées et defini le style
            GMarkerGoogle marker = new GMarkerGoogle(new PointLatLng(lat, lng), markerType);

            //On ajoute le point a notre element de carte
            markersOverlay.Markers.Add(marker);
            //On ajoute l'element a la carte
            carte.Overlays.Add(markersOverlay);

            //On ajoute le point au tableau de points
            points.Add(new PointLatLng(lat, lng));

            //On met a jour l'id courant du marker
            currentMarkerId++;

            //On ajoute au tableau de markers
            markers.Add(markersOverlay);
        }

        //Actualise la carte²f
        public void refreshMap()
        {
            //On deZoom et on Zoom afin de forcer la carte à s'actualiser
            carte.Zoom--;
            carte.Zoom++;
        }

        //Ajoute un polygone a la carte et le sauvegarde si true est passé en parametre
        public void addPolygone(bool save, int id = -1)//On passe un id si l'on veux specifier celui ci 
        {
            //Contient le nom du polygone
            string polygoneName = "";
            //On definit le nom du polygone
            if(id != -1)
            {
                polygoneName = "polygon_" + id;
            }
            else
            {
                polygoneName = "polygon_" + (util.getLastId() + 1);
            }


            //Defini le type d'element a crée sur notre carte
            GMapOverlay polyOverlay = new GMapOverlay(polygoneName);

            //Crée le polygone grace au tableau de points
            GMapPolygon polygon = new GMapPolygon(points, polygoneName);

            //Defini le style de l'element
            polygon.Fill = new SolidBrush(Color.FromArgb(70, polygoneColor));
            polygon.Stroke = new Pen(polygoneColor, 1);
            polygon.IsHitTestVisible = true;

            //On ajoute le polygone a notre element de carte
            polyOverlay.Polygons.Add(polygon);

            //On ajoute l'element a la carte
            this.carte.Overlays.Add(polyOverlay);

            //On ajoute le polygon au tableau
            polygons.Add(polygon);

            //On sauvegarde le polygone si true passé en parametre
            if (save)
            {
                //Sauvegarde les données
                util.writeData(points);
            }

            //On vide le tableau des points
            points = new List<PointLatLng>();

            //On reinitialise l'id du marker
            currentMarkerId = defaultId;

            //On actualise la carte
            refreshMap();
        }

        //Charge les polygones enregistrés
        public void loadData()
        {
            //On crée le fichier si il n'existe pas
            util.createFileIfDoesntExist(util.polygonsPointsfileName);
            util.createFileIfDoesntExist(util.polygonsFileName);

            //Variable qui contient les données au format List<string>
            List<string> data = util.getDataListString();

            //Variable qui contient l'id du polygone en cours de creation
            int currentId = defaultId;
            //Variable qui contient l'id du polygone en cours de traitement (peut par exemple contenir l'id 
            //du polygone 2 alors que le programme traite la polygone 1
            int tempId = defaultId;

            //On parcours toutes nos données
            foreach (string line in data)
            {
                //Si la ligne n'est pas vide
                if (line != "" && line != Environment.NewLine)
                {
                    //On met a jour l'id du polygone en cours de traitement
                    tempId = util.getId(line);
                    //Si le polygone en cours de traitement et le meme que le polygone en cours de creation
                    if (tempId == currentId)
                    {
                        //On ajoute un point au polygone courant
                        addPolygonePoint(util.getLatWithString(line), util.getLngWithString(line), tempId);
                    }
                    else//Si le polygone en cours de traitement n'est pas le meme que le polygone en cours de creation
                    {
                        //On crée le polygone courant 
                        addPolygone(false, currentId);
                        //On ajoute le point du nouveau polygone
                        addPolygonePoint(util.getLatWithString(line), util.getLngWithString(line), tempId);
                        //On met a jour l'id du polygone en cours de creation
                        currentId = tempId;
                    }
                }
            }
            //On ajoute le dernier polygone en cours de création
            addPolygone(false, currentId);
        }
    
        public string getPolygonIdByMarkerName(string name)
        {
            try
            {

                //On split le nom du marker
                string[] temp = name.Split('_');
                //On retourne le charactere a l'index 1
                return temp[1];

            }catch(Exception e) { }
            //En cas d'erreur
            return null;
        }

        public string getMarkerIdByMarkerName(string name)
        {
            try
            {

                //On split le nom du marker
                string[] temp = name.Split('_');
                //On retourne le charactere a l'index 2
                return temp[2];

            }
            catch (Exception e) { }
            //En cas d'erreur
            return null;
        }

        public void deletePolygon(GMapPolygon item, string polygoneName)
        {
            if(this.mode == 1)//Mode 1 = suppression
            {
                item.Dispose();
                string polygoneId = util.getPolygoneIdWithPolygoneName(polygoneName);
                Console.WriteLine(polygoneId);
                foreach (GMapOverlay marker in markers)
                {
                    if (polygoneId == util.getPolygoneMarkerIdWithMarkerName(marker.Id))
                    {
                        marker.Dispose();
                    }
                }
                util.deletePolygoneData(polygoneId);
            }
        }

        public void addDescriptionToPolygon(string desc, string polygoneName)
        {
            if(desc != "" && desc != null)
            {
                int polygoneId = Convert.ToInt32(util.getPolygoneIdWithPolygoneName(polygoneName));
                util.writePolygoneData(desc, polygoneId);
            }
        }

        public string getPolygoneDescriptionWithName(string polygoneName)
        {
            return util.getPolygoneDescriptionWithPolygoneName(polygoneName);
        }
    }
}
