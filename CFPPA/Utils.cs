﻿using GMap.NET;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CFPPA
{
    class Utils
    {
        //Variable qui permet de definir le separateur dans notre fichier csv
        private string separator;
        //Variable qui contient le nom du fichier qui contient les points des polygones
        public string polygonsPointsfileName;
        //Variable qui contient le nom du fichier qui contient les données des polygones
        public string polygonsFileName;

        public Utils()
        {
            //On definit le nom du fichier
            polygonsPointsfileName = "./polygonsPoints.csv";
            polygonsFileName = "./polygons.csv";
            //On definit le separateur dans notre fichier csv
            separator = ";";
        }

        //Enregistre les données dans un fichier au format csv points + description par defaut
        public void writeData(List<PointLatLng> points, string description = "Aucune description")
        {
            writePolygoneData(description);
            writePolygonsPointsData(points);            
        }

        //Ecrit les données du polygone
        public void writePolygoneData(string description, int id = -1)
        {
            string data = "";

            if(id == -1)
            {
                data = getDataCsv(polygonsFileName);
                data += (getLastId() + 1) + separator + description;
            }
            else
            {
                string newData = id + separator + description;
                data = getDataCsvWithReplacementData(Convert.ToString(id), newData, polygonsFileName);
            }

            //Permet decrire dans un fichier et le crée si non existant
            using (StreamWriter sw = File.CreateText(polygonsFileName))
            {
                //Ecriture des donnees dans le fichier  
                sw.WriteLine(data);
            }
        }

        //Ecrit les données des points de polygone
        private void writePolygonsPointsData(List<PointLatLng> points)
        {
            //On recupere le dernier id
            int id = getLastId() + 1;
            //On recupere les données au format csv
            string data = getDataCsv(polygonsPointsfileName);

            //Sert a definir la premiere ligne car celle-ci n'a pas de retour a la ligne
            bool firstPoint = true;
            //On parcours les points du polygones
            foreach (PointLatLng coord in points)
            {
                //Si premiere ligne
                if (firstPoint)
                {
                    //Les autres lignes ne sont plus la premiere
                    firstPoint = false;
                    //On ajoute les données a notre variable
                    data += id + separator + coord.Lat + separator + coord.Lng + separator;
                }//Si pas la premiere ligne
                else
                {
                    //On ajoute un retour a la ligne puis les données a notre variable
                    data += Environment.NewLine + id + separator + coord.Lat + separator + coord.Lng + separator;
                }
            }

            //Permet decrire dans un fichier et le crée si non existant
            using (StreamWriter sw = File.CreateText(polygonsPointsfileName))
            {
                //Ecriture des donnees dans le fichier  
                sw.WriteLine(data);
            }
        }
        
        //Crée le fichier s'il n'existe pas
        public void createFileIfDoesntExist(string filename)
        {
            if(!File.Exists(filename)){
                //Permet de crée si non existant
                using (StreamWriter sw = File.CreateText(filename))
                {}
            }
        }

        //Retourne l'id de la ligne envoyée
        public int getId(string line)
        {
            //Si la ligne en cours de traitement n'est pas vide
            if (line != "")
            {
                //On brise la variable avec le separateur définit
                String[] splitData = line.Split(';');
                //Try catch afin de prevenir les erreurs de conversions
                try
                {
                    //On retourne l'id de la ligne contenu dans la case 0 de notre tableau
                    return Convert.ToInt32(splitData[0]);
                }
                catch (Exception e) { }
            }
            //En cas d'erreur ou de ligne vide on retourne 0
            return 0;
        }

        //Retourne la latitude de la ligne envoyée
        public double getLatWithString(string line)
        {
            //On brise la variable avec le separateur définit
            string[] splitData = line.Split(';');
            //Try catch afin de prevenir les erreurs de conversions
            try
            {
                //On retourne la latitude de la ligne contenu dans la case 1 de notre tableau
                return Convert.ToDouble(splitData[1]);
            }
            catch (Exception e) { }

            //On retourne 0 en cas d'erreur 
            return 0;
        }

        //Retourne la longitude de la ligne envoyée
        public double getLngWithString(string line)
        {
            //On brise la variable avec le separateur définit
            string[] splitData = line.Split(';');
            //Try catch afin de prevenir les erreurs de conversions
            try
            {
                //On retourne la longitude de la ligne contenu dans la case 2 de notre tableau
                return Convert.ToDouble(splitData[2]);
            }
            catch (Exception e) { }

            //On retourne 0 en cas d'erreur 
            return -1;
        }

        //Retourne les données des polygones au fromat csv
        public string getDataCsv(string filename)
        {
            createFileIfDoesntExist(filename);
            //Variable qui va contenir les données
            string data = "";
            //Permet de lire un fichier grace a son nom
            using (StreamReader sr = File.OpenText(filename))
            {
                //Variable qui va contenir la ligne courante
                string line = "";
                //On parcours toutes les lignes du fichier
                while ((line = sr.ReadLine()) != null)
                {
                    //On ajoute les lignes a notre variable data
                    data += line + Environment.NewLine;
                }
            }
            //On retourne les données au format csv
            return data;
        }

        //Retourne les données des polygones au fromat csv
        public string getDataCsvWithExclusion(string polygoneIdToExclude, string filename)
        {
            //Variable qui va contenir les données
            string data = "";
            //Permet de lire un fichier grace a son nom
            using (StreamReader sr = File.OpenText(filename))
            {
                //Variable qui va contenir la ligne courante
                string line = "";
                //On parcours toutes les lignes du fichier
                while ((line = sr.ReadLine()) != null)
                {
                    if(getId(line).ToString() != polygoneIdToExclude)
                    {
                        //On ajoute les lignes a notre variable data
                        data += line + Environment.NewLine;
                    }
                }
            }
            //On retourne les données au format csv
            return data;
        }

        //Retourne les données des polygones au fromat csv
        public string getDataCsvWithReplacementData(string polygoneIdToReplace, string newData, string filename)
        {
            //Variable qui va contenir les données
            string data = "";
            //Permet de lire un fichier grace a son nom
            using (StreamReader sr = File.OpenText(filename))
            {
                //Variable qui va contenir la ligne courante
                string line = "";
                //On parcours toutes les lignes du fichier
                while ((line = sr.ReadLine()) != null)
                {
                    if (getId(line).ToString() != polygoneIdToReplace)
                    {
                        //On ajoute les lignes a notre variable data
                        data += line + Environment.NewLine;
                    }
                    else
                    {
                        data += newData + Environment.NewLine;
                    }
                }
            }
            //On retourne les données au format csv
            return data;
        }

        //Enregistre les données dans un fichier au format csv
        public void deletePolygoneData(string polygoneId)
        {
            //Suppression du fichier des points
            string data = getDataCsvWithExclusion(polygoneId, polygonsPointsfileName);
            //Permet decrire dans un fichier et le crée si non existant
            using (StreamWriter sw = File.CreateText(polygonsPointsfileName))
            {
                //Ecriture des donnees dans le fichier  
                sw.WriteLine(data);
            }
            //Supression du fichier des polygones
            data = getDataCsvWithExclusion(polygoneId, polygonsFileName);
            //Permet decrire dans un fichier et le crée si non existant
            using (StreamWriter sw = File.CreateText(polygonsFileName))
            {
                //Ecriture des donnees dans le fichier  
                sw.WriteLine(data);
            }
        }

        //retourne les données des polygones sous forme de List<string>
        public List<string> getDataListString()
        {
            //Variable (List<string) qui va contenir les données
            List<string> data = new List<string>();
            //Permet de lire un fichier grace a son nom
            using (StreamReader sr = File.OpenText(polygonsPointsfileName))
            {
                //Variable qui va contenir la ligne courante
                string line = "";
                //On parcours toutes les lignes du fichier
                while ((line = sr.ReadLine()) != null)
                {
                    //On ajoute les lignes a notre List<string> data
                    data.Add(line);
                }
            }
            //On retourne les données au format List<string>
            return data;
        }

        //Retourne le dernier id utiliser pour sauvegarder un polygon
        public int getLastId()
        {
            int lastId = 0;
            //Permet de lire un fichier grace a son nom
            using (StreamReader sr = File.OpenText(polygonsPointsfileName))
            {
                //Variable qui va contenir la derniere ligne de notre fichier
                string s = "";
                //Variable qui va contenir la ligne courante de notre fichier
                string temp = "";
                //On parcours toutes les lignes de notre fichier
                while ((temp = sr.ReadLine()) != null)
                {
                    //Si la ligne n'est pas vide
                    if (temp != Environment.NewLine && temp != "")
                    {
                        //On stocke la ligne courante dans la variable s
                        s = temp;
                    }
                }
                //On verifie par securite que la variable s n'est pas vide
                if (s != "" && s != Environment.NewLine)
                {
                    //On brise la variable s par le separateur definit
                    String[] data = s.Split(';');
                    //On retourne la case 0 autrement dit la case qui contient l'id du polygone
                    lastId = Convert.ToInt32(data[0]);
                }
            }
            //Si le fichier est vide on retourne 0
            return lastId;
        }

        //Retourne l'id d'un polygone avec son nom
        public string getPolygoneIdWithPolygoneName(string name)
        {
            try
            {
                string[] temp = name.Split('_');
                return temp[1];
            }catch(Exception e) { }
            return "";
        }
        //Retourne l'id d'un polygone avec son nom
        public string getPolygoneMarkerIdWithMarkerName(string name)
        {
            try
            {
                string[] temp = name.Split('_');
                return temp[1];
            }
            catch (Exception e) { }
            return "";
        }

        public string getPolygoneDescriptionWithPolygoneName(string polygoneName)
        {
            string polygoneId = getPolygoneIdWithPolygoneName(polygoneName);
            //Variable qui va contenir les données
            string description = "";
            //Permet de lire un fichier grace a son nom
            using (StreamReader sr = File.OpenText(polygonsFileName))
            {
                //Variable qui va contenir la ligne courante
                string line = "";
                //On parcours toutes les lignes du fichier
                while ((line = sr.ReadLine()) != null)
                {
                    if (getId(line).ToString() == polygoneId)
                    {
                        //On ajoute les lignes a notre variable data
                        description = getPolygoneDescriptionWithDataLine(line);
                        break;
                    }
                }
            }
            //On retourne les données au format csv
            return description;
        }

        public string getPolygoneDescriptionWithDataLine(string line)
        {
            //Si la ligne en cours de traitement n'est pas vide
            if (line != "")
            {
                //On brise la variable avec le separateur définit
                String[] splitData = line.Split(';');
                //Try catch afin de prevenir les erreurs de conversions
                try
                {
                    //On retourne l'id de la ligne contenu dans la case 0 de notre tableau
                    return splitData[1];
                }
                catch (Exception e) { }
            }
            //En cas d'erreur ou de ligne vide on retourne 0
            return "";
        }
    }
}
