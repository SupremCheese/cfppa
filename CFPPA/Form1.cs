﻿using GMap.NET;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;
using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace CFPPA
{
    public partial class Form1 : Form
    {
        //Objet GMap
        GoogleMap carte;
        Utils util;

        //Constructeur de notre classe
        public Form1()
        {
            this.util = new Utils();
            InitializeComponent();
            //On appelle la fonction pour initialiser la carte
            initMap();
        }

        //Initialise la carte avec les parametres par defaults
        private void initMap()
        {
            //On initialise notre objet GoogleMap
            carte = new GoogleMap(ref map, MouseButtons.Left, 5, 50, 10);
            //On charge les données dans la carte
            carte.loadData();
        }

        //Bouton Localiser, permet de centrer la carte via la latitude et la longitude
        private void setLocalisation_Click(object sender, EventArgs e)
        {            
            //Met a jour la position sur la carte
            map.Position = new GMap.NET.PointLatLng(Convert.ToDouble(lat.Text), Convert.ToDouble(lon.Text));
        }

        //Ajoute un point au polygone en fonction de l'emplacement de la souris
        private void map_MouseClick(object sender, MouseEventArgs e)
        {
            //Si clic droit
            if(e.Button == MouseButtons.Right && carte.getMode() == 0)
            {
                //On converti les coordonnées sur la carte en latitude et longitude
                var point = map.FromLocalToLatLng(e.X, e.Y);

                //On ajoute un point au polygone courant
                carte.addPolygonePoint(point.Lat, point.Lng);

                //On rafraichi la carte
                carte.refreshMap();
            }
        }

        //Permet d'associer des actions a une touche du clavier
        private void map_keyPress(object sender, KeyPressEventArgs e)
        {
            //Si on appuie sur la touche Entré
            if(e.KeyChar == Convert.ToChar(Keys.Enter) && carte.getMode() == 0)
            {
                //On crée le polygone avec les points courants
                carte.addPolygone(true);
            }
        }

        //Boutton pour changer de mode
        private void changeMode(object sender, EventArgs e)
        {
            carte.changeMode(carte.getMode() + 1);
            int mode = carte.getMode();
            switch (mode)
            {
                case 0:
                    changeModeButton.Text = "Mode Ajout";
                    break;
                case 1:
                    changeModeButton.Text = "Mode Suppression";
                    break;                
                case 2:
                    changeModeButton.Text = "Mode Description";
                    break;
            }
        }

        private void map_onPolygonClick(GMapPolygon item, MouseEventArgs e)
        {
            switch (carte.getMode())
            {
                case 1:
                    carte.deletePolygon(item, item.Name);
                    break;
                case 2:
                    carte.addDescriptionToPolygon(Interaction.InputBox("Description de la parcelle", "Description", carte.getPolygoneDescriptionWithName(item.Name)), item.Name);
                    break;
            }
        }
    }
}
