﻿namespace CFPPA
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.map = new GMap.NET.WindowsForms.GMapControl();
            this.changeModeButton = new System.Windows.Forms.Button();
            this.setLocalisation = new System.Windows.Forms.Button();
            this.lon = new System.Windows.Forms.TextBox();
            this.Longitude = new System.Windows.Forms.Label();
            this.lat = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(0, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 450);
            this.splitter1.TabIndex = 0;
            this.splitter1.TabStop = false;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(3, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.map);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.changeModeButton);
            this.splitContainer1.Panel2.Controls.Add(this.setLocalisation);
            this.splitContainer1.Panel2.Controls.Add(this.lon);
            this.splitContainer1.Panel2.Controls.Add(this.Longitude);
            this.splitContainer1.Panel2.Controls.Add(this.lat);
            this.splitContainer1.Panel2.Controls.Add(this.label1);
            this.splitContainer1.Size = new System.Drawing.Size(797, 450);
            this.splitContainer1.SplitterDistance = 657;
            this.splitContainer1.TabIndex = 1;
            // 
            // map
            // 
            this.map.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.map.Bearing = 0F;
            this.map.CanDragMap = true;
            this.map.EmptyTileColor = System.Drawing.Color.Navy;
            this.map.GrayScaleMode = false;
            this.map.HelperLineOption = GMap.NET.WindowsForms.HelperLineOptions.DontShow;
            this.map.LevelsKeepInMemmory = 5;
            this.map.Location = new System.Drawing.Point(9, 12);
            this.map.MarkersEnabled = true;
            this.map.MaxZoom = 2;
            this.map.MinZoom = 2;
            this.map.MouseWheelZoomEnabled = true;
            this.map.MouseWheelZoomType = GMap.NET.MouseWheelZoomType.MousePositionAndCenter;
            this.map.Name = "map";
            this.map.NegativeMode = false;
            this.map.PolygonsEnabled = true;
            this.map.RetryLoadTile = 0;
            this.map.RoutesEnabled = true;
            this.map.ScaleMode = GMap.NET.WindowsForms.ScaleModes.Integer;
            this.map.SelectedAreaFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(65)))), ((int)(((byte)(105)))), ((int)(((byte)(225)))));
            this.map.ShowTileGridLines = false;
            this.map.Size = new System.Drawing.Size(645, 426);
            this.map.TabIndex = 0;
            this.map.Zoom = 0D;
            this.map.OnPolygonClick += new GMap.NET.WindowsForms.PolygonClick(this.map_onPolygonClick);
            this.map.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.map_keyPress);
            this.map.MouseClick += new System.Windows.Forms.MouseEventHandler(this.map_MouseClick);
            // 
            // changeModeButton
            // 
            this.changeModeButton.Location = new System.Drawing.Point(7, 124);
            this.changeModeButton.Name = "changeModeButton";
            this.changeModeButton.Size = new System.Drawing.Size(117, 23);
            this.changeModeButton.TabIndex = 5;
            this.changeModeButton.Text = "Mode Ajout";
            this.changeModeButton.UseVisualStyleBackColor = true;
            this.changeModeButton.Click += new System.EventHandler(this.changeMode);
            // 
            // setLocalisation
            // 
            this.setLocalisation.Location = new System.Drawing.Point(7, 94);
            this.setLocalisation.Name = "setLocalisation";
            this.setLocalisation.Size = new System.Drawing.Size(117, 23);
            this.setLocalisation.TabIndex = 4;
            this.setLocalisation.Text = "Localiser";
            this.setLocalisation.UseVisualStyleBackColor = true;
            this.setLocalisation.Click += new System.EventHandler(this.setLocalisation_Click);
            // 
            // lon
            // 
            this.lon.Location = new System.Drawing.Point(7, 68);
            this.lon.Name = "lon";
            this.lon.Size = new System.Drawing.Size(117, 20);
            this.lon.TabIndex = 3;
            // 
            // Longitude
            // 
            this.Longitude.AutoSize = true;
            this.Longitude.Location = new System.Drawing.Point(4, 52);
            this.Longitude.Name = "Longitude";
            this.Longitude.Size = new System.Drawing.Size(54, 13);
            this.Longitude.TabIndex = 2;
            this.Longitude.Text = "Longitude";
            // 
            // lat
            // 
            this.lat.Location = new System.Drawing.Point(7, 29);
            this.lat.Name = "lat";
            this.lat.Size = new System.Drawing.Size(117, 20);
            this.lat.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Latitude";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.splitter1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private GMap.NET.WindowsForms.GMapControl map;
        private System.Windows.Forms.TextBox lon;
        private System.Windows.Forms.Label Longitude;
        private System.Windows.Forms.TextBox lat;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button setLocalisation;
        private System.Windows.Forms.Button changeModeButton;
    }
}

